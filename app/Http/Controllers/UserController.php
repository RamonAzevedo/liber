<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function createUser(Request $request) {
        $user = new User;
        $user->name = $request->name;
        $user->adress = $request->adress;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->date_of_birth = $request->date_of_birth;
        $user->save();
        return response()->json(['user' => $user],200);
    }
    public function index() {
    	$users = User::all();
    	return response()->json(['users' => $users],200);
	}

	public function show($id_usuario) {
    	$user = User::find($id_usuario);
    	return response()->json(['user' => $user],200);
    }
    
    public function update(Request $request, $id_usuario) {
    	$user = User::find($id_usuario);
    	if($request->name){
        	$user->name = $request->name;
        }
        
        if($request->adress){
        	$user->adress = $request->adress;
    	}

    	if($request->author){
        	$user->author = $request->author;
    	}

    	if($request->password){
        	$user->password = $request->password;
        }

        if($request->date_of_birth){
        	$user->date_of_birth = $request->date_of_birth;
    	}
    	$user->save();
    	return response()->json(['user' => $user],200);
    }
    public function destroy($id_usuario) {
    	User::destroy($id_usuario);

    	return response()->json(['Usuario deletado com sucesso'],200);
	}
}
