<?php

namespace App\Http\Controllers;
use App\Models\Book;

use Illuminate\Http\Request;

class BookController extends Controller
{
    public function createBook(Request $request) {
        $book = new Book;
        $book->title = $request->title;
        $book->author = $request->author;
        $book->price = $request->price;
        $book->category = $request->category;
        $book->status = $request->status;
        $book->number_of_pages = $request->number_of_pages;
        $book->isbn = $request->isbn;
        $book->photo = $request->photo;
        $book->save();
        return response()->json(['book' => $book],200);
    }
    public function index() {
    	$books = Book::all();
    	return response()->json(['books' => $books],200);
	}

	public function show($id_livro) {
    	$book = Book::find($id_livro);
    	return response()->json(['book' => $book],200);
    }
    
    public function update(Request $request, $id_livro) {
    	$book = Book::find($id_livro);
    	if($request->title){
        	$book->title = $request->title;
    	}

    	if($request->author){
        	$book->author = $request->author;
    	}

    	if($request->price){
        	$book->price = $request->price;
        }
        if($request->category){
        	$book->category = $request->category;
        }
        if($request->status){
        	$book->status = $request->status;
        }
        if($request->number_of_pages){
        	$book->number_os_pages = $request->number_of_pages;
        }
        if($request->isbn){
        	$book->isbn = $request->isbn;
        }
        if($request->photo){
        	$book->photo = $request->photo;
    	}
    	$book->save();
    	return response()->json(['book' => $book],200);
    }
    public function destroy($id_livro) {
    	Book::destroy($id_livro);

    	return response()->json(['Livro deletado com sucesso'],200);
	}
}
